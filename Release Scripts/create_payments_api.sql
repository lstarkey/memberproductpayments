CREATE OR REPLACE PACKAGE payments_api AS

    PROCEDURE create_due_payments;
    
END payments_api;
/
CREATE OR REPLACE PACKAGE BODY payments_api AS
    
    PROCEDURE logger_prc (
        i_severity IN NUMBER,
        i_message IN VARCHAR2,
        i_sqlcode IN NUMBER,
        i_sqlerror VARCHAR2) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO proc_logs (prl_severity, prl_message, prl_sqlcode, prl_sqlerror)
            VALUES(i_severity, i_message, i_sqlcode, i_sqlerror);
        COMMIT;
    END logger_prc;
    
    PROCEDURE create_due_payments AS
        l_sqlcode NUMBER;
        l_sqlerror VARCHAR2(512);
        -- local variables for member_products
        CURSOR c_get_mbpr IS
            SELECT  mbpr_id,
                    mbpr_mbr_id,
                    mbpr_prod_id,
                    mbpr_payment_due
            FROM member_products
            WHERE mbpr_payment_due <= CAST(LOCALTIMESTAMP AS TIMESTAMP)
            FOR UPDATE;
        t_mbpr c_get_mbpr%ROWTYPE;
        -- local variables for products
        l_prod_cost products.prod_cost%TYPE;
        -- local variables for payments
        l_new_payment_due TIMESTAMP;
    BEGIN
        logger_prc(1, 'Starting create_due_payments Procedure', '', '');
        OPEN c_get_mbpr;
        LOOP
            FETCH c_get_mbpr
                INTO t_mbpr;
            EXIT WHEN c_get_mbpr%NOTFOUND;
            -- get product cost from prod_id - unique values
            SELECT prod_cost
                INTO l_prod_cost
                FROM products
                WHERE prod_id = t_mbpr.mbpr_prod_id;
            -- create payment record
            logger_prc(1, ('Creating Payment Record for pay_mbpr_id = '||t_mbpr.mbpr_id||', pay_payment_date = '||t_mbpr.mbpr_payment_due||', pay_amount = '||l_prod_cost), '', '');
            INSERT INTO payments (pay_mbpr_id, pay_payment_date, pay_amount)
            VALUES (t_mbpr.mbpr_id, t_mbpr.mbpr_payment_due, l_prod_cost);
            -- update payment due date to 1 year ahead from original date
            l_new_payment_due := ADD_MONTHS(t_mbpr.mbpr_payment_due, 12);
            logger_prc(1, 'Updating Member_Product Record, old mbpr_payment_due = '||t_mbpr.mbpr_payment_due||', new mbpr_payment_due = '||l_new_payment_due, '', '');
            UPDATE member_products SET mbpr_payment_due = l_new_payment_due WHERE CURRENT OF c_get_mbpr; 
        END LOOP;
        IF c_get_mbpr%ISOPEN THEN
            CLOSE c_get_mbpr;
        END IF;
        logger_prc(1, 'Committing create_due_payments changes to database', '', '');
        COMMIT;        
    EXCEPTION
        WHEN OTHERS THEN
        l_sqlcode := SQLCODE;
        l_sqlerror := SQLERRM;
        logger_prc(3, 'Exception occurred in create_due_payments, rolling back all transaction', l_sqlcode, l_sqlerror);
        ROLLBACK;
        RAISE;
    END create_due_payments;

END payments_api;