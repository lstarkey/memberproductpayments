CREATE TABLE products (
    prod_id NUMBER(10) NOT NULL,
    prod_name VARCHAR2(100) NOT NULL,
    prod_cost NUMBER(9,0) NOT NULL,
    CONSTRAINT prod_pk PRIMARY KEY (prod_id),
    CONSTRAINT prod_name_uk UNIQUE (prod_name));
/
CREATE SEQUENCE prod_seq 
    START WITH 1
    INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER prod_id_trg
    BEFORE INSERT ON products
    FOR EACH ROW
BEGIN
    SELECT prod_seq.NEXTVAL
    INTO :new.prod_id
    FROM dual;
END;
/
CREATE TABLE members (
    mbr_id NUMBER(10) NOT NULL, -- id to be used as membership number
    mbr_forename VARCHAR2(50) NOT NULL,
    mbr_surname VARCHAR2(50) NOT NULL,
    CONSTRAINT mbr_pk PRIMARY KEY (mbr_id));
/
CREATE SEQUENCE mbr_seq 
    START WITH 100023 -- don't want to start with membership number 1
    INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER mbr_id_trg
    BEFORE INSERT ON members
    FOR EACH ROW
BEGIN
    SELECT mbr_seq.NEXTVAL
    INTO :new.mbr_id
    FROM dual;
END;
/
CREATE TABLE member_products (
    mbpr_id NUMBER(10) NOT NULL,
    mbpr_mbr_id NUMBER(10) NOT NULL,
    mbpr_prod_id NUMBER(10) NOT NULL,
    mbpr_payment_due TIMESTAMP DEFAULT LOCALTIMESTAMP NOT NULL, 
    CONSTRAINT mbpr_pk PRIMARY KEY (mbpr_id),
    CONSTRAINT mbpr_mbr_fk FOREIGN KEY (mbpr_mbr_id) REFERENCES members (mbr_id),
    CONSTRAINT mbpr_prod_fk FOREIGN KEY (mbpr_prod_id) REFERENCES products (prod_id));
/
CREATE SEQUENCE mbpr_seq 
    START WITH 1
    INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER mbpr_id_trg
    BEFORE INSERT ON member_products
    FOR EACH ROW
BEGIN
    SELECT mbpr_seq.NEXTVAL
    INTO :new.mbpr_id
    FROM dual;
END;
/
CREATE TABLE payments (
    pay_id NUMBER(10) NOT NULL,
    pay_mbpr_id NUMBER(10) NOT NULL,
    pay_payment_date TIMESTAMP NOT NULL,
    pay_amount NUMBER(9,0) NOT NULL,
    CONSTRAINT pay_pk PRIMARY KEY (pay_id),
    CONSTRAINT pay_mbpr_fk FOREIGN KEY (pay_mbpr_id) REFERENCES member_products (mbpr_id));
/
CREATE SEQUENCE pay_seq 
    START WITH 1
    INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER pay_id_trg
    BEFORE INSERT ON payments
    FOR EACH ROW
BEGIN
    SELECT pay_seq.NEXTVAL
    INTO :new.pay_id
    FROM dual;
END;
/
CREATE TABLE proc_log_severities (
    pls_id NUMBER(1),
    pls_description VARCHAR2(11),
    CONSTRAINT pls_pk PRIMARY KEY (pls_id));
/
INSERT ALL
    INTO proc_log_severities (pls_id, pls_description) VALUES (1,'Information')
    INTO proc_log_severities (pls_id, pls_description) VALUES (2,'Warning')
    INTO proc_log_severities (pls_id, pls_description) VALUES (3,'Error')
    INTO proc_log_severities (pls_id, pls_description) VALUES (4,'Critical')
SELECT * FROM dual;
COMMIT;
/
CREATE TABLE proc_logs (
    -- no id column for logs table, not expecting any foreign key constraints to this
    -- to consider: how is log data being retrieved and which column(s) should be indexed? 
    prl_date TIMESTAMP DEFAULT LOCALTIMESTAMP NOT NULL,
    prl_severity NUMBER(1),
    prl_message VARCHAR2(512),
    prl_sqlcode NUMBER,
    prl_sqlerror VARCHAR2(512),
    CONSTRAINT prl_pls_fk FOREIGN KEY (prl_severity) REFERENCES proc_log_severities (pls_id));
/