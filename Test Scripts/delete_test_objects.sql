DROP SEQUENCE prod_seq;   
DROP SEQUENCE mbr_seq;
DROP SEQUENCE mbpr_seq;
DROP SEQUENCE pay_seq;
DROP TABLE payments;
DROP TABLE member_products;
DROP TABLE members;
DROP TABLE products;
DROP TABLE proc_logs;
DROP TABLE proc_log_severities;
DROP SEQUENCE LUKE_TEST;
COMMIT;