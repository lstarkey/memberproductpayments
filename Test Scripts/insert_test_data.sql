DECLARE
    -- local var to check if sequence exists
    l_seq_count NUMBER(1) := 0;
BEGIN
    -- create sequence used for test data population if necessary
    SELECT COUNT(*) 
        INTO l_seq_count
        FROM USER_SEQUENCES 
        WHERE SEQUENCE_NAME = 'LUKE_TEST';
    IF l_seq_count = 0 THEN
        EXECUTE IMMEDIATE 'CREATE SEQUENCE LUKE_TEST START WITH 1';
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Sequence created: LUKE_TEST');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Sequence already exists: LUKE_TEST');
    END IF;
END;
/
DECLARE
    -- exception handler variables
    l_sqlcode NUMBER;
    l_sqlerror VARCHAR2(512);
    -- local variables for iterating round insert statements
    l_counter NUMBER(10) := 0;
    -- set l_max to number of rows of test data to insert into each table
    -- for test purposes each member will have 1 product each and then add a second product for evey other member so we have some with more than 1
    l_max NUMBER(10) := 1000;
    -- local variable for setting unique test product names, appending unique number
    l_seq NUMBER(10);
    -- local variables for generating test name data for member inserts
    l_array_forename dbms_sql.varchar2_table;
    l_array_surname dbms_sql.varchar2_table;
    l_forename VARCHAR2(50);
    l_surname VARCHAR2(50);
    l_random PLS_INTEGER;
    -- local variables for creating member_product test data
    CURSOR c_get_members IS
        SELECT mbr_id
        FROM members;
    t_member c_get_members%ROWTYPE;
    l_prod_id NUMBER(10);
BEGIN
    -- populate products with test data
    LOOP
        l_counter := l_counter + 1;
        l_seq := luke_test.NEXTVAL;
        INSERT INTO products (PROD_NAME, PROD_COST)
        VALUES ('TestProduct'||l_seq,l_seq); -- Using sequence because of unique constraint on name column
        EXIT WHEN l_counter = l_max;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Inserted '||l_counter||' rows of test data into products table.');
    
    -- reset counter
    l_counter := 0;
    -- populate members with test data
    -- set up array of test forenames to pick randomly for insert
    l_array_forename(1) := 'Luke';
    l_array_forename(2) := 'Jon';
    l_array_forename(3) := 'Tony';
    l_array_forename(4) := 'Bella';
    l_array_forename(5) := 'Tilda';
    l_array_forename(6) := 'Anthony';
    l_array_forename(7) := 'Edward';
    l_array_forename(8) := 'Vicky';
    l_array_forename(9) := 'Samantha';
    l_array_forename(10) := 'Maeve';
    -- set up array of test surnames to pick randomly for insert
    l_array_surname(1) := 'Starkey';
    l_array_surname(2) := 'Thompson';
    l_array_surname(3) := 'Myers';
    l_array_surname(4) := 'May';
    l_array_surname(5) := 'Rawsthorne';
    l_array_surname(6) := 'Cleese';
    l_array_surname(7) := 'Palin';
    l_array_surname(8) := 'Gilliam';
    l_array_surname(9) := 'Idle';
    l_array_surname(10) := 'Chapman';
    LOOP
        l_counter := l_counter + 1;
        l_random := DBMS_RANDOM.VALUE(1,10);
        l_forename := l_array_forename(l_random);
        l_random := DBMS_RANDOM.VALUE(1,10);
        l_surname := l_array_surname(l_random);
        INSERT INTO members (mbr_forename, mbr_surname)
        VALUES (l_forename, l_surname);
        EXIT WHEN l_counter = l_max;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Inserted '||l_counter||' rows of test data into members table.');
    
    -- populate member_products with test data - 1 row for each member - note if run multiple times this will add more member_products for existing members
    OPEN c_get_members;
    LOOP
        FETCH c_get_members
            INTO t_member;
        EXIT WHEN c_get_members%NOTFOUND;
        -- now select a product to use for the member product, has to have a valid foreign key so query all and sort randomly then select the top one. 
        -- not efficient because it's retrieving all records in each loop but avoids issues with any gaps in the prod_id's from other testing/failures. Will do for testing. 
        SELECT prod_id 
            INTO l_prod_id
            FROM (SELECT * FROM products
            ORDER BY DBMS_RANDOM.VALUE)
                WHERE ROWNUM = 1;        
        --DBMS_OUTPUT.PUT_LINE('l_prod_id is '||l_prod_id);
        INSERT INTO member_products (mbpr_mbr_id, mbpr_prod_id, mbpr_payment_due)
        VALUES (t_member.mbr_id, l_prod_id, (LOCALTIMESTAMP + DBMS_RANDOM.VALUE(-366, 366)));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Total members found: '||c_get_members%ROWCOUNT||'. Inserted '||c_get_members%ROWCOUNT||' rows of test data into member_products table.');
    IF c_get_members%ISOPEN THEN
        CLOSE c_get_members;
    END IF;
    
    COMMIT;
    
EXCEPTION
    WHEN OTHERS THEN
    l_sqlcode := SQLCODE;
    l_sqlerror := SQLERRM;
    DBMS_OUTPUT.PUT_LINE('SQLCODE: '||l_sqlcode);
    DBMS_OUTPUT.PUT_LINE('SQLERRM: '||l_sqlerror);
    ROLLBACK;
END;

