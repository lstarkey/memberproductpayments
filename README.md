# MemberProductPayments

Release Scripts directory contains release scripts for production

Test scripts directory contains initial test scripts for this project

Assumptions:
- creating tables and objects within an existing database/schema - schema not specified explicitly in any scripts.
- rows in each table will not exceed 9999999999 (10 digits).
- max product price in pence will not exceed 999,999,999 or £9,999,999.99
- acceptable to use ID column of members table as the membership number, sequence created with high initial value so nobody has a membership number of 1.
- Any additional department references etc. to be added programmatically to the membership number. 
- do not require time zones, not used internationally.
- using payment due date from the member_products table as the payment date to populate payments table with. 

Testing:

- script/sequence created to populate the tables with basic test data (see Test Scripts folder) - data is required to test the insertion of payment records, creation of logs, updating member payment records.
- initial basic testing can be conducted by running queries after insertion of test data to inspect number of records due for payment and compare to logs and records processed in the member_products and payments tables after running the procedure.  
- script also created to delete tables and objects to start from scratch during development.
- initial testing for inserting records etc. using dbms_output to check. 
- create reusable unit tests to run each time the schema/procedure/package is changed. 

Initial Enhancement Ideas/Clarifications:

- does the payments table need a column for payment received, the pay_payment_date field is populated from the payment due date at the time of creating the record. What is the downstream process? 
- product price is taken from the products table at the point of creating the payment records, what happens with any price variations over time/offers etc.? Might need to populate the member_products table with a price at the time of purchasing/renewing? 
- no id column, primary key or indexes defined on the proc_logs table, need to understand how data is being accessed. 